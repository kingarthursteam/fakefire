-- ================= --
--  Basic functions  --
-- ================= --

--[[
	This function checks for the presence of air up to 'height' above 'pos'
	@param pos The startpoint as table {x=,y=,z=}
	@param height Up to how many nodes should be checked. Number
	@return true if all nodes from pos to hight is air, false otherwise
]]
fake_fire.check_for_air = function(pos, height)
	local pos = {x=pos.x, y=pos.y, z=pos.z} --generate a local copy of pos
	for i = 1 , height do
		if minetest.get_node(pos).name ~= "air" then
			return false -- If there is no air, return false
		end
		pos.y = pos.y+1
	end
	return true -- Return true if the full height is only air
end

--[[
	This function plays the fake_fire_extinguish sound on punch
]]
fake_fire.extinguish = function(pos, node, puncher)
	minetest.sound_play("fake_fire_extinguish", {pos = pos, gain = 1.0, max_hear_distance = 10,})
end
