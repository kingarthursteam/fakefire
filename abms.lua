

-- ~~~~~~~~~~~~~~~~~~~~~~~~
-- DRIFTING SMOKE
-- ~~~~~~~~~~~~~~~~~~~~~~~~

-- WHITE, BOTTOM SMOKE
-- Closest to fire, hotter, faster, more dense
minetest.register_abm({
	nodenames = {
				"group:smoky_d",
				},
	interval = 1,
	chance = 1,
	action = function(pos, node)
	if
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z}, 2)
	then
		local white_or_grey = math.random(2)
		minetest.add_particlespawner({
			amount = 1,
			time = 1,
			minpos = {x=pos.x-0.1, y=pos.y+0.5, z=pos.z-0.3},
			maxpos = {x=pos.x+0.1, y=pos.y+1.0, z=pos.z+0.1},
			minvel = {x=-0.1, y=0.3, z=-0.3},
			maxvel = {x=0.1, y=0.6, z=-0.5},
			minacc = {x=0.0,y=0.3,z=-0.3},
			maxacc = {x=0.0,y=0.6,z=-0.5},
			minexptime = 1,
			maxexptime = 3,
			minsize = 8,
			maxsize = 12,
			collisiondetection = true,
			texture = "smoke_"..white_or_grey..".png"
			})
		end
	end
})



-- GREY, MIDDLE SMOKE
-- Cooler, slower, not as dense.
minetest.register_abm({
	nodenames = {
				"group:smoky_d",
				},
-- The white and black smoke interval/chance are 1/2.
-- Grey smoke is in the middle and has to be there to connect the two.
-- So it's interval/chance is 1/1.
-- ~ LazyJ, 2014_07_28
	interval = 1,
	chance = 2,
	action = function(pos, node)
	local mgrey_or_dgrey = math.random(2)
	-- The nodes.lua file does a space check for this area to determine whether
	-- to use drifting smoke or non-drifting smoke. That *would* be enough *if*
	-- nothing is built in this area *after* the smoke has been set in motion.
	-- So I added an extra space check here just for that possibility.
	-- ~ LazyJ, 2014_07_31
	if
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z-1}, 3)
	then
		minetest.add_particlespawner({
			amount = 1,
			time = 1,
			minpos = {x=pos.x-0.2, y=pos.y+0.5, z=pos.z-1.0},
			maxpos = {x=pos.x+0.2, y=pos.y+1.0, z=pos.z-0.5},
			minvel = {x=-0.1, y=0.3, z=-0.3},
			maxvel = {x=0.1, y=0.6, z=-0.5},
			minacc = {x=0.0,y=0.3,z=-0.3},
			maxacc = {x=0.0,y=0.6,z=-0.5},
			minexptime = 1,
			maxexptime = 6,
			minsize = 6,
			maxsize = 11,
			collisiondetection = true,
			texture = "smoke2_"..mgrey_or_dgrey..".png"
			})
		end
	end
})



-- BLACK, TOP SMOKE
-- Farthest from the fire, coolest, slowest, more spread out.
minetest.register_abm({
	nodenames = {
				"group:smoky_d",
				},
	interval = 1,
	chance = 4,
	action = function(pos, node)
	if
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z-1}, 7) and
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z-2}, 7) and
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z-3}, 7) and
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z-4}, 7) and
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z-5}, 7)
	then
		minetest.add_particlespawner({
			amount = 1,
			time = 1,
			minpos = {x=pos.x-0.4, y=pos.y+0.5, z=pos.z-0.4},
			maxpos = {x=pos.x+0.4, y=pos.y+1.0, z=pos.z+0.4},
			minvel = {x=-0.1, y=0.3, z=-0.3},
			maxvel = {x=0.1, y=0.6, z=-0.5},
			minacc = {x=0.0,y=0.3,z=-0.3},
			maxacc = {x=0.0,y=0.6,z=-0.5},
			minexptime = 2,
			maxexptime = 4,
			minsize = 6,
			maxsize = 10,
			collisiondetection = true,
			texture = "black_smoke.png"
			})
		end
	end
})



-- ~~~~~~~~~~~~~~~~~~~~~~~~
-- NON-DRIFTING SMOKE
-- ~~~~~~~~~~~~~~~~~~~~~~~~

-- WHITE, BOTTOM SMOKE
-- Closest to fire, hotter, faster, more dense
minetest.register_abm({
	nodenames = {
				"group:smoky_v",
				},
	interval = 1,
	chance = 1,
	action = function(pos, node)
	if
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z}, 2)
	then
		local white_or_grey = math.random(2)
		minetest.add_particlespawner({
			amount = 1,
			time = 1,
			minpos = {x=pos.x, y=pos.y+0.2, z=pos.z},
			maxpos = {x=pos.x, y=pos.y+1.5, z=pos.z},
			minvel = {x=0.0, y=0.1, z=0.0},
			maxvel = {x=0.0, y=0.6, z=0.0},
			minacc = {x=0.0,y=0.1,z=0.0},
			maxacc = {x=0.0,y=0.6,z=0.0},
			minexptime = 1.0,
			maxexptime = 3.0,
			minsize = 7,
			-- a maxsize larger than 10 and the biggest smoke particle gets stuck
			-- in a 1x1 chimney flue. ~ LazyJ, 2014_07_30
			maxsize = 10,
			collisiondetection = true,
			texture = "smoke_"..white_or_grey..".png"
			--texture = "top_03.png"
			})
		end
	end
})



-- GREY, MIDDLE SMOKE
-- Cooler, slower, not as dense.
minetest.register_abm({
	nodenames = {
				"group:smoky_v",
				},
-- The white and black smoke interval/chance are 1/2.
-- Grey smoke is in the middle and has to be there to connect the two.
-- So it's interval/chance is 1/1.
-- ~ LazyJ, 2014_07_28
	interval = 1,
	chance = 3,
	action = function(pos, node)
	if
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z}, 5)
	then
	local mgrey_or_dgrey = math.random(2)
		minetest.add_particlespawner({
			amount = 1,
			time = 8,
			minpos = {x=pos.x, y=pos.y+0.2, z=pos.z},
			maxpos = {x=pos.x, y=pos.y+1.5, z=pos.z},
			minvel = {x=0.0, y=0.4, z=0.0},
			maxvel = {x=0.0, y=1.0, z=0.0},
			minacc = {x=0.0,y=0.4,z=0.0},
			maxacc = {x=0.0,y=1.0,z=0.0},
			minexptime = 3,
			maxexptime = 7,
			minsize = 6,
			-- a maxsize larger than 10 and the biggest smoke particle gets stuck
			-- in a 1x1 chimney flue. ~ LazyJ, 2014_07_30
			maxsize = 9,
			collisiondetection = true,
			texture = "smoke2_"..mgrey_or_dgrey..".png"
		--	texture = "top_02.png"
			})
		end
	end
})



-- BLACK, TOP SMOKE
-- Farthest from the fire, coolest, slowest, more spread out.
minetest.register_abm({
	nodenames = {
				"group:smoky_v",
				},
	interval = 1,
	chance = 4,
	action = function(pos, node)
	if
		fake_fire.check_for_air({x=pos.x, y=pos.y+1, z=pos.z}, 7)
	then
		minetest.add_particlespawner({
			amount = 1,
			time = 1,
			minpos = {x=pos.x, y=pos.y+0.2, z=pos.z},
			maxpos = {x=pos.x, y=pos.y+1.5, z=pos.z},
			minvel = {x=0.0, y=0.9, z=0.0},
			maxvel = {x=0.0, y=1.5, z=0.0},
			minacc = {x=0.0,y=1.5,z=0.0},
			maxacc = {x=0.0,y=0.9,z=0.0},
			minexptime = 1,
			maxexptime = 5,
			minsize = 4,
			maxsize = 7,
			collisiondetection = true,
			texture = "black_smoke.png"
			--texture = "top_01.png"
			})
		end
	end
})
