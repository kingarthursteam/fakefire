-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~ --
-- FAKE FIRE MOD - Version 1.0 --
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~ --

fake_fire = {}; -- Global fake_fire namespace

local MODPATH = minetest.get_modpath("fake_fire")..DIR_DELIM
dofile(MODPATH.."functions.lua")
dofile(MODPATH.."abms.lua")
fake_fire_water = true;

local cold_nodes = {
	["default:snowblock"]=true,
	["default:ice"]=true,
	["default:dirt_with_snow"]=true,
}
-- function for fire --
local function set_fake_fire(pointed_thing, itemstack, wear)
	local u = minetest.get_node(pointed_thing.under)
	local n = minetest.get_node(pointed_thing.above)

	-- if the node is air then --
	if  cold_nodes[u.name] and n.name == "air" then
		minetest.set_node(pointed_thing.above,
		{name="fake_fire:fake_ice_fire"})
		-- then play the sound --
		minetest.sound_play("fake_fire_light", {
			pos = pointed_thing.above,
			max_hear_distance = 10,
			gain = 10.0,
		})
		-- degrade the item --
		itemstack:add_wear(wear)

		--end
	elseif n.name == "air" then
		-- create fire above that node --
		minetest.set_node(pointed_thing.above,
		{name="fake_fire:fake_fire"})
		-- then play the sound --
		minetest.sound_play("fake_fire_light", {
			pos = pointed_thing.above,
			max_hear_distance = 10,
			gain = 10.0,
		})
		-- degrade the item --
		itemstack:add_wear(wear)
	end

end


-- abm for putting out fire --

if fake_fire_water == true then
	minetest.register_abm({
		nodenames = {"fake_fire:fake_fire"},
		neighbors = {"group:puts_out_fire"},
		interval = 3,
		chance = 1,
		catch_up = false,
		action = function(p0, node, _, _)
			minetest.remove_node(p0)
			minetest.sound_play("fake_fire_extinguish",
				{pos = p0, max_hear_distance = 10, gain = 0.25})
		end,
	})
end

local function check_area(pos,node1,node2)

	if
		-- 'If' the space directly south and a 14 nodes up from the flame or
		-- chimney_top is free and clear,... ~LazyJ
		--minetest.get_node({x=pos.x, y=pos.y-1, z=pos.z-1}).name == "air" and
		fake_fire.check_for_air({x=pos.x, y=pos.y, z=pos.z-1}, 14)
	then
		-- 'then' set the node that triggers the drifting-smoke ABM. ~ LazyJ
		minetest.set_node(pos, {name = node1 })
	else
		-- 'else' set the node that triggers the non-drifing-smoke ABM. ~ LazyJ
		minetest.set_node(pos, {name = node2 })
	end

end


-- =========== --
-- other Nodes --
-- =========== --

-- embers --

minetest.register_node("fake_fire:embers", {
	description = "Glowing Embers",
	tiles = {
		{name="embers_animated.png", animation={type="vertical_frames",
		aspect_w=16, aspect_h=16, length=2}},
		},
	is_ground_content = false,
	light_source = 9,
	groups = {choppy=3, crumbly=3, oddly_breakable_by_hand=3},
})

-- invisable chimly --

minetest.register_node("fake_fire:chimney_invs_d", {
	description = "Chimney Top - invisable",
	tiles = {"chimney_invs.png"},
	inventory_image = "black_smoke.png",
	is_ground_content = true,
	groups = {cracky=3, oddly_breakable_by_hand=1, smoky_d = 1},
	paramtype = "light",
	walkable = false,
	drawtype = "glasslike",
})

-- =================== --
--  New CHIMNEY NODES  --
-- =================== --

local function register_chimV(name, node, description)

	minetest.register_node("fake_fire:chimney_" ..name.."_v", {
		description = "Chimney Top - " .. description,
		tiles = {"default_"..node..".png^chimney_top.png", "default_"..node..".png"},
		is_ground_content = true,
		groups = {cracky=3, oddly_breakable_by_hand=1, not_in_creative_inventory=1, smoky_v = 1},
		paramtype = "light",
		--sounds = default.node_sound_stone_defaults(),
		drop = "fake_fire:chimney_"..node,
		drawtype = "nodebox",
		node_box = {
		  type = "fixed",
		  fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5},
		  },
		on_rightclick = function (pos,node,puncher)
			-- This swaps the smokeless version with the smoky version when punched.
			-- ~ LazyJ
			minetest.set_node(pos, {name = "fake_fire:chimney_"..name})
		end
	})

end

local function register_chimD(name, node, description)

	minetest.register_node("fake_fire:chimney_" ..name.."_d", {
		description = "Chimney Top - " .. description,
		tiles = {"default_"..node..".png^chimney_top.png", "default_"..node..".png"},
		is_ground_content = true,
		groups = {cracky=3, oddly_breakable_by_hand=1, not_in_creative_inventory=1, smoky_d = 1},
		paramtype = "light",
		--sounds = default.node_sound_stone_defaults(),
		drop = "fake_fire:chimney_" ..name,
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5},
			},
		on_rightclick = function (pos,node,puncher)
			-- This swaps the smokeless version with the smoky version when punched.
			-- ~ LazyJ
			minetest.set_node(pos, {name = "fake_fire:chimney_"..name})
		end
	})

end

local function register_chim(name, node, description)

	minetest.register_node("fake_fire:chimney_" ..name, {
		description = "Chimney Top - " .. description,
	  	tiles = {"default_"..node..".png^chimney_top.png", "default_"..node..".png"},
		is_ground_content = true,
		groups = {cracky=3, oddly_breakable_by_hand=1},
		paramtype = "light",
		--sounds = default.node_sound_stone_defaults(),
		drop = "fake_fire:chimney_" ..name,
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5},
		},
		on_rightclick = function (pos,node,puncher)
			-- Check if there is likely enough space for the smoke to drift without
			-- passing through chimneys, walls, or other solids.  ~ LazyJ, 2014_07_28
			check_area(pos,"fake_fire:chimney_"..name.."_d", "fake_fire:chimney_"..name.."_v",puncher)
		end
	})

	minetest.register_craft({
		type = "shapeless",
		output = 'fake_fire:chimney_'..name.. ' 2',
		recipe = {
				"default:torch",
				"default:"..node,
		}
	})

	-- minetest.register_alias("fake_fire:chimney_" ..name , "fake_fire:chimney_" ..name.. "D", "fake_fire:chimney_" ..name.. "V")

end

local nodes = {
	-- RGB Prime Colours --

	-- MonoChrome --
	{name="stone", node="stone_block" , description="Stone"},
	{name="sandstone", node="sandstone_block", description="SandStone"},
	{name="desert_stone", node="desert_stone_block", description="Desert Stone"},
	{name="desert_sandstone", node="desert_sandstone_block", description="Desert SandStone"},
	{name="silver_sandstone", node="silver_sandstone_block", description="Silver SandStone"},
	{name="brick", node="brick", description="Brick"},
}

for i,node in ipairs(nodes) do
	register_chim(node.name, node.node, node.description)
	register_chimD(node.name, node.node, node.description)
	register_chimV(node.name, node.node, node.description)
	-- register whatever here
end


-- =============== --
--  new Fake Fire  --
-- =============== --

local function register_fireD(name, description)

	minetest.register_node("fake_fire:fake_"..name.."_d", {
		description = "fake_".. description,
		tiles = {
			{name="fake_"..name.."_animated.png",
			animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=1.5}},
			},
		paramtype = "light",
		is_ground_content = true,
		inventory_image = "fake_"..name..".png",
		drawtype = "firelike",
		buildable_to = true,
		light_source = 14,
		drop = '', -- fire cannot be picked up
		damage_per_second = 0, -- default 0 Recomended 2*0.5, --
		groups = {dig_immediate=3,attached_node=1,not_in_creative_inventory=1,smoky_d = 1},
		paramtype = "light",
		walkable = false,
		on_punch = fake_fire.extinguish,
		on_rightclick = function (pos,node,puncher)
			-- swap somke fire to normal fire --
			minetest.set_node(pos, {name = "fake_fire:fake_"..name, })
		end
	})

end

local function register_fireV(name, description)

	minetest.register_node("fake_fire:fake_"..name.."_v", {
		description = "fake_".. description,
		tiles = {
			{name="fake_"..name.."_animated.png",
			animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=1.5}},
		},
		paramtype = "light",
		is_ground_content = true,
		inventory_image = "fake_"..name..".png",
		drawtype = "firelike",
		buildable_to = true,
		light_source = 14,
		drop = '', -- fire cannot be picked up
		damage_per_second = 0, -- default 0 Recomended 2*0.5, --
		groups = {dig_immediate=3,attached_node=1,not_in_creative_inventory=1,smoky_v = 1},
		paramtype = "light",
		walkable = false,
		on_punch = fake_fire.extinguish,
		on_rightclick = function (pos,node,puncher)
			-- swap somke fire to normal fire --
			minetest.set_node(pos, {name = "fake_fire:fake_"..name, })
		end
	})

end


local function register_fire(name, description)

	minetest.register_node("fake_fire:fake_"..name, {
	  description = "fake_".. description,
		tiles = {
			{name="fake_"..name.."_animated.png",
			animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=1.5}},
			},
		paramtype = "light",
		is_ground_content = true,
		inventory_image = "fake_"..name..".png",
		drawtype = "firelike",
		buildable_to = true,
		light_source = 14,
		drop = '', -- fire cannot be picked up
		damage_per_second = 0, -- default 0 Recomended 2*0.5, --
		groups = {dig_immediate=3,attached_node=1},
		paramtype = "light",
		walkable = false,
		on_punch = fake_fire.extinguish,
		on_rightclick = function (pos,node,puncher)
			check_area(pos,"fake_fire:fake_"..name.."_d", "fake_fire:fake_"..name.."_v")
		end
	})
	-- minetest.register_alias("fake_fire:fake_" ..name , "fake_fire:fake_" ..name.. "D", "fake_fire:fake_" ..name.. "V")
end

local fires = {
	-- RGB Prime Colours --

	-- MonoChrome --
	{name="fire", description="Fire"},
	{name="ice_fire", description="Ice Fire"},
}

for i,fire in ipairs(fires) do
	register_fire(fire.name, fire.description)
	register_fireD(fire.name, fire.description)
	register_fireV(fire.name, fire.description)
	-- register whatever here
end

-- ========== --
-- Fire Tools --
-- ========== --

-- Flint and Steel --

--list = { "default:snow" = true, "default:ice"=true, "default:dirt_with_snow" }

minetest.register_tool("fake_fire:flint_and_steel", {
	description = "Fake Fire Lighter",
	inventory_image = "flint_and_steel.png",
	liquids_pointable = false,
	stack_max = 1,
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={
			flamable = {uses=65, maxlevel=1},
		}
	},
	on_use = function(itemstack, user, pointed_thing)
	-- if item is pointing at a node then --
	if pointed_thing.type == "node" then
		-- set a value the degradeing --
		wear = 65535/65
		-- load function on line 6 --
		set_fake_fire(pointed_thing,itemstack,wear)
		return itemstack
		end
	end,
})

-- Old Flint and Steel --

--[[ Players cannot craft this item, they will need to spawn it via
	 "/give" or creative.
]]--

minetest.register_tool("fake_fire:old_flint_and_steel", {
	description = "Never Ending Flint & Steel",
	inventory_image = "flint_and_steel.png",
	liquids_pointable = false,
	stack_max = 1,
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={
			flamable = {uses=65, maxlevel=1},
		}
	},
	on_use = function(itemstack, user, pointed_thing)
	if pointed_thing.type == "node" then
		-- this is so that it will never brake --
		wear = 0
		-- load function on line 6 --
		set_fake_fire(pointed_thing, itemstack, wear)
		return itemstack
		end
	end,
})

-- =========== --
-- Fire crafts --
-- =========== --

minetest.register_craft({
	output = "fake_fire:flint_and_steel",
	recipe = {
		{"default:flint", ""},
		{"", "default:gold_ingot"},
	}
})
minetest.register_craft({
	output = "fake_fire:flint_and_steel",
	recipe = {
		{"default:flint", "default:gold_ingot"},
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'fake_fire:embers',
	recipe = {
			"default:torch",
			"group:wood",
	}
})

-- old support --

minetest.register_alias("fake_fire:smokeless_chimney_top_stone", "fake_fire:chimney_stone")
minetest.register_alias("fake_fire:chimney_top_stone_d", "fake_fire:chimney_stone_d")
minetest.register_alias("fake_fire:chimney_top_stone", "fake_fire:chimney_stone_v")

minetest.register_alias("fake_fire:smokeless_chimney_top_sandstone", "fake_fire:chimney_sandstone")
minetest.register_alias("fake_fire:chimney_top_sandstone_d", "fake_fire:chimney_sandstone_d")
minetest.register_alias("fake_fire:chimney_top_sandstone", "fake_fire:chimney_sandstone_v")

minetest.register_alias("fake_fire:fake_fire_h", "fake_fire:fake_fire_v")
