# Media Licence # 

## Textures ##

Textures are created by myself and are licneced under the Creative Commons 4.0 

[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

## Sound Licences ##

All sounds are from freesounds.org

### fire lighter ###

Licenced under the Creative Commons 3.0 by JasonElrod 

[CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)

https://freesound.org/people/JasonElrod/sounds/85457/

### fire extinguish ###

Licenced under the Creative Commons 4.0 by 14GPanskaLetko_Dominik

[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

https://freesound.org/people/14GPanskaLetko_Dominik/sounds/419290/
